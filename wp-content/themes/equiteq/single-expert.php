<?php

get_header();
$id = get_the_ID();
$expert = get_expert($id);
// $industry_expertises = maybe_unserialize($expert->industry_expertise);

$profileImage = get_field('profile_image', $id);
$profileImageUrl = $profileImage['url'];
$profileImageAlt = $profileImage['alt'];
$profileName = get_the_title($id);
$profileDesignation = get_field('title', $id);
$profileLocationObj = get_field('location', $id);
$profileLocationId = $profileLocationObj->ID;
$profileLocation = $profileLocationObj->post_title;
$profileEmail = get_field('email', $id);
$profileContact = get_field('contact_no', $id);
$profileLinkedIn = get_field('linkedin', $id);
$profileDescription = get_field('description', $id);
?>


<section>
    <div class="container no-pad-gutters">
        <div class="back mb-4 mb-md-5">
            <i class="fa fa-caret-left align-bottom" style="font-size: 22px;" aria-hidden="true"></i> <a
                href="<?=site_url( '/team/', 'http' );?>" class="btn-outline-success text-uppercase px-0 ml-2">Back to team</a>
        </div>
        <!--May implement the expert's profile here -->
        <div class="row">
            <div class="col-lg-12 mb-4 expert-details">
                <div class="team-two-col">
                    <div class="team-left">
                        <div class="team-bg-img">
                            <img src="<?=esc_url($profileImageUrl);?>" alt="<?=esc_attr($profileImageAlt);?>">
                        </div>
                    </div>
                    <div class="team-right">
                        <div class="profile-title-in">
                            <h1><?=esc_html($profileName);?></h1>
                        </div>
                        <div class="profile-designation">
                            <h6><?=esc_attr($profileDesignation);?></h6>
                        </div>
                        <div class="city-title">
                            <p>
                                <i class="fa fa-map-marker fa-lg text-green" aria-hidden="true"></i>
                                <?=esc_attr($profileLocation);?>
                            </p>
                        </div>
                        <div class="social-icon">
                            <ul>
                                <li><a href="mailto:<?=esc_attr($profileEmail);?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                <li><a href="tel:<?=esc_attr($profileContact);?>"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                                <li><a href="<?=esc_url($profileLinkedIn);?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                                </li>
                            </ul>
                        </div>
                        <div class="team-profile-con">
                            <?=$profileDescription;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!--May implement the expert's industry expertise here -->

<?php
get_footer();