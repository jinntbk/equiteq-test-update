const profileFilters = {
    industry: '',
    location: '',
    profileName: '',
};

const filterIndustry = document.querySelector("#typeListLabelSector");
const filterListIndustry = document.querySelector(".sector-filter .typeListing");
const filterListItemIndustry = document.querySelectorAll(".sector-filter .typeListing li");
const filterLocation = document.querySelector("#typeListLabelLocation");
const filterListLocation = document.querySelector(".location-filter .typeListing");
const filterListItemLocation = document.querySelectorAll(".location-filter .typeListing li");
const filterSearch = document.querySelector("#quicksearch");

filterIndustry.addEventListener("click", (event) => {
    let displaySetting = filterListIndustry.style.display;
    if (displaySetting == 'block') {
        filterListIndustry.style.display = "none";
    }
    else {
        filterListIndustry.style.display = "block";
    }
});

filterLocation.addEventListener("click", (event) => {
    let displaySetting = filterListLocation.style.display;
    if (displaySetting == 'block') {
        filterListLocation.style.display = "none";
    }
    else {
        filterListLocation.style.display = "block";
    }
});

filterListItemIndustry.forEach((item) => {
    item.addEventListener("click", (event) => {
        profileFilters.industry = event.target.getAttribute('data-value');
        filterListIndustry.style.display = "none";

        let label = 'Sector';
        if(profileFilters.industry !== null && profileFilters.industry !== '') {
            label = event.target.innerText;
        }
        document.querySelector("#typeListLabelSector span").innerText = label;

        do_filter_experts();
    });
});

filterListItemLocation.forEach((item) => {
    item.addEventListener("click", (event) => {
        profileFilters.location = event.target.getAttribute('data-value');
        filterListLocation.style.display = "none";

        let label = 'Location';
        if(profileFilters.location !== null && profileFilters.location !== '') {
            label = event.target.innerText;
        }
        document.querySelector("#typeListLabelLocation span").innerText = label;
        
        do_filter_experts();
    });
});

filterSearch.addEventListener("keyup", (event) => {
    profileFilters.profileName = event.target.value;
    do_filter_experts();
});

function do_filter_experts() {
    let industry = profileFilters.industry;
    let location = profileFilters.location;
    let searchTerm = profileFilters.profileName;

    jQuery.ajax({
            type: "POST",
            dataType: "html",
            url: wp_ajax.ajax_url,
            data: {
                action: "filter_experts",
                location: location,
                industry: industry,
                searchTerm: searchTerm,
            },
            success: function(response) {
                jQuery('.expert-list').html(response);
            },
            error: function(result) {
                console.warn(result);
            }
    });
}


jQuery(document).ready( function($) {
    
});