<?php
$profileId = get_the_ID();
$profilePageUrl = get_permalink($profileId);
$profileImage = get_field('profile_image', $profileId);
$profileImageUrl = $profileImage['url'];
$profileImageAlt = $profileImage['alt'];
$profileName = get_the_title($profileId);
$profileDesignation = get_field('title', $profileId);
$profileLocationObj = get_field('location', $profileId);
$profileLocationId = $profileLocationObj->ID;
$profileLocation = $profileLocationObj->post_title;
$profileEmail = get_field('email', $profileId);
$profileContact = get_field('contact_no', $profileId);
$profileLinkedIn = get_field('linkedin', $profileId);
$profileIndustryExpertiseObj = get_field('industry_expertise', $profileId);
$profileIndustryExpertiseId = (isset($profileIndustryExpertiseObj) && !empty($profileIndustryExpertiseObj)) ? $profileIndustryExpertiseObj[0]->ID : '';
?>
<div class="profile-box" data-industry="<?= $profileIndustryExpertiseId; ?>" data-location="<?= $profileLocationId; ?>">
    <div class="profile-box-inner">
        <div class="profile-img">
            <a href="<?=esc_url($profilePageUrl);?>">
                <img src="<?=esc_url($profileImageUrl);?>" alt="<?=esc_attr($profileImageAlt);?>">              
            </a>
        </div>
        <div class="profile-name">
            <a href="<?=esc_url($profilePageUrl);?>"><?=esc_html($profileName);?></a>
        </div>
        <div class="profile-designation"><?=esc_attr($profileDesignation);?></div>
        <div class="profile-location"><?=esc_attr($profileLocation);?></div>
        <div class="profile-social-icons">
            <ul class="experts-socials p-0 align-items-center">
                <li><a href="mailto:<?=esc_attr($profileEmail);?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                <li><a href="tel:<?=esc_attr($profileContact);?>"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                <li><a href="<?=esc_url($profileLinkedIn);?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
</div>